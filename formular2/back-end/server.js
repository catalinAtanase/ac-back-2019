const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());

const port = 8080;
app.listen(port, () => {
    console.log("Server online on: " + port);
});

app.use('/', express.static('../front-end'))

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "siscit_abonament"
});

connection.connect(function (err) {

    console.log("Connected to database!");
    const sql = "CREATE TABLE IF NOT EXISTS abonament(nume VARCHAR(40),prenume VARCHAR(40),telefon VARCHAR(15),email VARCHAR(50),facebook VARCHAR(60),tipAbonament VARCHAR(20),nrCard VARCHAR(40),cvv INTEGER, varsta VARCHAR(3), cnp VARCHAR(20), sex VARCHAR(1), UNIQUE (email));";
    connection.query(sql, function (err, result) {
        // console.log('err');

        if (err) throw err;
    });
});

app.post("/abonament", (req, res) => {
    let nume = req.body.nume;
    let prenume = req.body.prenume;
    let telefon = req.body.telefon;
    let email = req.body.email;
    let facebook = req.body.facebook;
    let tipAbonament = req.body.abonament;
    let nrCard = req.body.nrCard;
    let cvv = req.body.cvv;
    let varsta = req.body.varsta;
    let cnp = req.body.cnp;
    let error = []

    if (!nume || !prenume || !telefon || !email || !facebook || !tipAbonament || !nrCard || !cvv || !varsta || !cnp) {
        error.push('Unul dintre campuri nu a fost completat!')
    }

    if (nume.length < 3 || nume.length > 20 || !nume.match('^[A-Za-z]+$')) {
        error.push('Nume invalid!')
    }

    if (prenume.length < 3 || prenume.length > 20 || !prenume.match('^[A-Za-z]+$')) {
        error.push('Prenume invalid!')
    }

    if (telefon.length != 10 || !telefon.match('^[0-9]+$')) {
        error.push('Numar de telefon invalid!')
    }

    if (!email.toLowerCase().includes("@gmail.com") && !email.toLowerCase().includes("@yahoo.com")) {
        error.push("Email invalid!");
    }

    if (!facebook.includes('https://www.facebook.com/')) {
        error.push('Facebook invalid!')
    }

    if (nrCard.length != 16 || !nrCard.match('^[0-9]+$')) {
        error.push('Card invalid!')
    }

    if (cvv.length != 3 || !cvv.match('^[0-9]+$')) {
        error.push('Card invalid!')
    }

    if (varsta.length < 1 || varsta.length > 3 || !varsta.match('^[0-9]+$')) {
        error.push('Varsta invalida')
    }

    if (cnp.length != 13 || !cnp.match('^[0-9]+$')) {
        error.push('Varsta invalida')

    }

    let anulNasterii

    // Cea mai batrana persoana are 116 ani, deci nu este posibil sa mai existe cnp cu 3, 4

    if (cnp.substring(0, 1) == '1' || cnp.substring(0, 1) == '2') {
        anulNasterii = `19${cnp.substring(1, 3)}`
    } else {
        anulNasterii = `20${cnp.substring(1, 3)}`
    }

    let luna = cnp.substring(3, 5)
    let zi = cnp.substring(5, 7)

    let today = new Date()

    let varstaCalculata = today.getFullYear() - anulNasterii
    let lunaCalculata = today.getMonth() - luna + 1
    let ziCalculata = today.getDate() - zi

    if (lunaCalculata < 0 || (lunaCalculata == 0 && ziCalculata < zi)) {
        varstaCalculata--
    }

    if (varsta != varstaCalculata) {
        error.push('Varsta nu corespunde!')
    }

    if (cnp[0] == 0 || cnp[0] == 9 || cnp[0] == 7 || cnp[0] == 8) {
        error.push('CNP invalid!')
    } else {
        if (cnp[0] == 1 || cnp[0] == 3 || cnp[0] == 5) {
            sex = 'M'
        } else if (cnp[0] == 2 || cnp[0] == 4 || cnp[0] == 6) {
            sex = 'F'
        } 
    }

    let sql = `Select 1 from abonament where email = '${email}'`

    connection.query(sql, (err, result) => {
        result.forEach(r => {
            if (err)
                throw err;
            if (r['1'] == 1) {
                error.push('Adresa de email deja folosita!');
            }
        });

        if (error.length === 0) {
            const sql =
                "INSERT INTO abonament (nume,prenume,telefon,email,facebook,tipAbonament,nrCard,cvv, varsta, cnp, sex) VALUES('" + nume + "','" + prenume + "','" + telefon + "','" + email + "','" + facebook + "','" + tipAbonament + "','" + nrCard + "','" + cvv + "','" + varsta + "','" + cnp + "','" + sex + "')";
            connection.query(sql, function (err, result) {
                if (err) throw err;
                console.log("Abonament achizitionat!");
            });
            res.status(200).send({
                message: "Abonament achizitionat!"
            });

            console.log(sql);
        } else {
            res.status(500).send(error)
            console.log("Eroare la inserarea in baza de date!");
        }
    })
});